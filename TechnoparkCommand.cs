﻿using System.Collections.Generic;
using Rhino;
using Rhino.Commands;
using Rhino.Geometry;
using Rhino.Input.Custom;
using Rhino.Input;
using System;

namespace Technopark
{
    [System.Runtime.InteropServices.Guid("4ee445c3-fb50-41db-aa87-1d6850131446")]
    public class TechnoparkCommand : Command
    {
        private Transform transRotation;
        private Transform transTranslation;
        private Transform transScale;

        public TechnoparkCommand()
        {
            // Rhino only creates one instance of each command class defined in a
            // plug-in, so it is safe to store a refence in a static property.
            Instance = this;
        }

        ///<summary>The only instance of this command.</summary>
        public static TechnoparkCommand Instance
        {
            get; private set;
        }

        ///<returns>The command name as it appears on the Rhino command line.</returns>
        public override string EnglishName
        {
            get { return "DrawTechnopark"; }
        }

        protected override Result RunCommand(RhinoDoc doc, RunMode mode)
        {
            // TODO: start here modifying the behaviour of your command.
            // ---
            RhinoApp.WriteLine("The {0} command will add a line right now.", EnglishName);

            Point3d pt0;
            using (GetPoint getPointAction = new GetPoint())
            {
                getPointAction.SetCommandPrompt("Please select the start point");
                if (getPointAction.Get() != GetResult.Point)
                {
                    RhinoApp.WriteLine("No start point was selected.");
                    return getPointAction.CommandResult();
                }
                pt0 = getPointAction.Point();
            }

            Point3d pt1;
            using (GetPoint getPointAction = new GetPoint())
            {
                getPointAction.SetCommandPrompt("Please select the end point");
                getPointAction.SetBasePoint(pt0, true);
                getPointAction.DynamicDraw +=
                  (sender, e) => e.Display.DrawLine(pt0, e.CurrentPoint, System.Drawing.Color.DarkRed);
                if (getPointAction.Get() != GetResult.Point)
                {
                    RhinoApp.WriteLine("No end point was selected.");
                    return getPointAction.CommandResult();
                }
                pt1 = getPointAction.Point();
            }

            Line firstSide = new Line(pt0, pt1);
            Vector3d v = firstSide.Direction;

            List<Line> technopark = new List<Line>();
            transRotation = Transform.Rotation(new Vector3d(0, 1, 0), v, new Point3d(0, 0, 0));
            transTranslation = Transform.Translation(pt0.X, pt0.Y, pt0.Z);
            transScale = Transform.Scale(new Point3d(0, 0, 0), firstSide.Length);

            technopark.Add(new Line(new Point3d(0, 0, 0), new Point3d(0, 1, 0)));
            technopark.Add(new Line(new Point3d(0, 1, 0), new Point3d(1, 1, 0)));
            technopark.Add(new Line(new Point3d(1, 1, 0), new Point3d(1, 0, 0)));
            technopark.Add(new Line(new Point3d(1, 0, 0), new Point3d(0, 0, 0)));

            technopark.Add(new Line(new Point3d(0.4, 0, 1.3), new Point3d(0.4, 1, 1.3)));
            technopark.Add(new Line(new Point3d(0.4, 1, 1.3), new Point3d(1.6, 1, 1.3)));
            technopark.Add(new Line(new Point3d(1.6, 1, 1.3), new Point3d(1.6, 0, 1.3)));
            technopark.Add(new Line(new Point3d(1.6, 0, 1.3), new Point3d(0.4, 0, 1.3)));

            technopark.Add(new Line(new Point3d(1.1, 0, 1.1), new Point3d(1.1, 1, 1.1)));
            technopark.Add(new Line(new Point3d(1.1, 1, 1.1), new Point3d(1.6, 1, 1.1)));
            technopark.Add(new Line(new Point3d(1.6, 1, 1.1), new Point3d(1.6, 0, 1.1)));
            technopark.Add(new Line(new Point3d(1.6, 0, 1.1), new Point3d(1.1, 0, 1.1)));

            technopark.Add(new Line(new Point3d(1.4, 1, 1.3), new Point3d(1.6, 1, 1.3)));
            technopark.Add(new Line(new Point3d(1.6, 1, 1.3), new Point3d(1.6, 1.5, 1.3)));
            technopark.Add(new Line(new Point3d(1.6, 1.5, 1.3), new Point3d(1.4, 1.5, 1.3)));
            technopark.Add(new Line(new Point3d(1.4, 1.5, 1.3), new Point3d(1.4, 1, 1.3)));

            technopark.Add(new Line(new Point3d(1.4, 1, 1.1), new Point3d(1.6, 1, 1.1)));
            technopark.Add(new Line(new Point3d(1.6, 1, 1.1), new Point3d(1.6, 1.5, 1.1)));
            technopark.Add(new Line(new Point3d(1.6, 1.5, 1.1), new Point3d(1.4, 1.5, 1.1)));
            technopark.Add(new Line(new Point3d(1.4, 1.5, 1.1), new Point3d(1.4, 1, 1.1)));

            technopark.Add(new Line(new Point3d(1.4, 1.5, 1.1), new Point3d(1.9, 1.5, 1.1)));
            technopark.Add(new Line(new Point3d(1.9, 1.5, 1.1), new Point3d(1.9, 2.5, 1.1)));
            technopark.Add(new Line(new Point3d(1.9, 2.5, 1.1), new Point3d(1.4, 2.5, 1.1)));
            technopark.Add(new Line(new Point3d(1.4, 2.5, 1.1), new Point3d(1.4, 1.5, 1.1)));

            technopark.Add(new Line(new Point3d(1.4, 1.5, 1.3), new Point3d(2.6, 1.5, 1.3)));
            technopark.Add(new Line(new Point3d(2.6, 1.5, 1.3), new Point3d(2.6, 2.5, 1.3)));
            technopark.Add(new Line(new Point3d(2.6, 2.5, 1.3), new Point3d(1.4, 2.5, 1.3)));
            technopark.Add(new Line(new Point3d(1.4, 2.5, 1.3), new Point3d(1.4, 1.5, 1.3)));

            technopark.Add(new Line(new Point3d(2, 1.5, 0), new Point3d(3, 1.5, 0)));
            technopark.Add(new Line(new Point3d(3, 1.5, 0), new Point3d(3, 2.5, 0)));
            technopark.Add(new Line(new Point3d(3, 2.5, 0), new Point3d(2, 2.5, 0)));
            technopark.Add(new Line(new Point3d(2, 2.5, 0), new Point3d(2, 1.5, 0)));

            technopark.Add(new Line(new Point3d(0, 0, 0), new Point3d(0.4, 0, 1.3)));
            technopark.Add(new Line(new Point3d(0, 1, 0), new Point3d(0.4, 1, 1.3)));

            technopark.Add(new Line(new Point3d(1, 0, 0), new Point3d(1.1, 0, 1.1)));
            technopark.Add(new Line(new Point3d(1, 1, 0), new Point3d(1.1, 1, 1.1)));

            technopark.Add(new Line(new Point3d(2, 1.5, 0), new Point3d(1.9, 1.5, 1.1)));
            technopark.Add(new Line(new Point3d(2, 2.5, 0), new Point3d(1.9, 2.5, 1.1)));

            technopark.Add(new Line(new Point3d(3, 1.5, 0), new Point3d(2.6, 1.5, 1.3)));
            technopark.Add(new Line(new Point3d(3, 2.5, 0), new Point3d(2.6, 2.5, 1.3)));

            drawLines(technopark, doc);

            return Result.Success;
        }

        private void drawLines(List<Line> lines, RhinoDoc doc)
        {
            Line cur;
            for (int i = 0; i < lines.Count; i++)
            {
                cur = lines[i];
                cur.Transform(transRotation);
                cur.Transform(transScale);
                cur.Transform(transTranslation);
                doc.Objects.AddLine(cur);
            }
            doc.Views.Redraw();
        }
    }
}
